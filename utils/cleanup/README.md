# Cleanup Scripts


####del_all_params.sh

Deletes all parameters in the parameter store for a given region.

    Usage : 
        region=<regionname> ./del_all_params.sh
    Example: 
        region=eu-west-1 ./del_all_params.sh


